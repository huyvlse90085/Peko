@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <div class="login-container relative">
            <section class="banner-slider owl-carousel">
                <img src="images/banner/banner.jpg" alt="" title="">
                <img src="images/banner/banner-2.jpg" alt="" title="">
            </section>
            <section class="login-form">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 offset-md-8">
                            <div class="block-customer-login" id="result_register">
                                <img class="logo-form" src="images/logo/logo.png" alt="" title="">
                                <h1 class="pages-title"><span>@lang('label.header.Signup')</span></h1>
                                <form class="form-login" action="" id="form_register">
                                    {{ csrf_field() }}
                                    <fieldset class="fieldset-login">
                                        <div class="field phone required">
                                            <div class="controls relative">
                                                <input type="text" name="phone" placeholder="@lang('label.For-shop.Phone')" required>
                                                <div class="absolute flex-center country">
                                                    <div id="show-country" class="show-country">
                                                        <span class="sing"></span>
                                                    </div>
                                                    <select class="choice-country">
                                                        <option value="sing">+65</option>
                                                        <option value="vn">+84</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field username required">
                                            <label>@lang('label.For-shop.Fullname')</label>
                                            <input type="text" name="fullName" required>
                                        </div>
                                        <div class="field password required">
                                            <label>@lang('label.For-shop.Password')</label>
                                            <input type="password" name="password" required>
                                        </div>
                                        <div class="actions-toolbar">
                                            <button id="btn_register" type="submit" class="btn submit">@lang('label.header.Signup')</button>
                                        </div>
                                        <div class="pslogin-login">
                                            <a class="login-facebook ps-social" href="">
                                                <span class="pslogin-icon"><i class="fa fa-facebook"
                                                                              aria-hidden="true"></i></span>
                                                <span class="pslogin-detail">@lang('label.SignUp.Facebook')</span>
                                            </a>
                                            <a class="login-google ps-social" href="">
                                                <span class="pslogin-icon"><i class="fa fa-google-plus"
                                                                              aria-hidden="true"></i></span>
                                                <span class="pslogin-detail">@lang('label.SignUp.Google')</span>
                                            </a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection