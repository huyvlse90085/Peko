<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Config extends Model
{
    protected $table = 'configs';

    public static function all($columns = ['*'])
    {
        return parent::select('*', 'configs.content_' . App::getLocale() . ' as content')->all($columns);
    }

    public static function getContent($key)
    {
        return parent::select('content_' . App::getLocale() . ' as content')
            ->where('is_activated', true)
            ->where('slug', $key)
            ->first();
    }
}
